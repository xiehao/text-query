//
// Created by oaheix on 14/12/2017.
//

#include "not_query.hh"
#include "query_result.hh"

using namespace std;

QueryResult NotQuery::evaluate(TextQuery const &text) const {
    auto result = query_.evaluate(text);
    auto return_lines = make_shared<lines_type>();
    auto begin = result.begin();
    auto end = result.end();
    auto size = result.file()->size();
    for (int i = 0; i < size; ++i) {
        if (begin == end || *begin != i) {
            return_lines->insert(i);
        } else if (begin != end) {
            ++begin;
        }
    }
    return {representation(), return_lines, result.file()};
}

std::string NotQuery::representation() const {
    return "~(" + query_.representation() + ")";
}
