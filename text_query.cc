//
// Created by xiehao on 17-12-13.
//


#include "text_query.hh"

#include <sstream>

#include "query_result.hh"

using namespace std;

TextQuery::TextQuery(std::ifstream &is)
        : file_{make_shared<vector<string>>()} {
    string text;
    while (getline(is, text)) {
        file_->push_back(text);
        auto n = file_->size() - 1;
        istringstream line{text};
        string word;
        while (line >> word) {
            auto &lines = word_map_[word];
            if (!lines) { lines.reset(new lines_type); }
            lines->insert(n);
        }
    }
}

QueryResult TextQuery::query(std::string const &sought) const {
    static auto no_data = make_shared<lines_type>();
    auto location = word_map_.find(sought);
    return word_map_.end() == location ?
           QueryResult{sought, no_data, file_} :
           QueryResult{sought, location->second, file_};
}
