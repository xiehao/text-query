//
// Created by oaheix on 14/12/2017.
//

#ifndef TEXTQUERY_OR_QUERY_HH
#define TEXTQUERY_OR_QUERY_HH


#include "binary_query.hh"

class OrQuery : public BinaryQuery {
    friend Query operator|(Query const &lhs, Query const &rhs);

    OrQuery(Query const &lhs, Query const &rhs)
            : BinaryQuery{lhs, rhs, "|"} {}

    QueryResult evaluate(TextQuery const &text) const override;
};

inline Query operator|(const Query &lhs, const Query &rhs) {
    return Query{std::shared_ptr<QueryBase>(new OrQuery{lhs, rhs})};
}


#endif //TEXTQUERY_OR_QUERY_HH
