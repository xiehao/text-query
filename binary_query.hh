//
// Created by oaheix on 14/12/2017.
//

#ifndef TEXTQUERY_BINARY_QUERY_HH
#define TEXTQUERY_BINARY_QUERY_HH


#include <utility>

#include "query_base.hh"
#include "query.hh"

class BinaryQuery : public QueryBase {
protected:
    BinaryQuery(Query lhs, Query rhs, std::string operator_symbol)
            : lhs_{std::move(lhs)},
              rhs_{std::move(rhs)},
              operator_symbol_{std::move(operator_symbol)} {}

    std::string representation() const override {
        return "(" + lhs_.representation() + " "
               + operator_symbol_ + " "
               + rhs_.representation() + ")";
    }

    Query lhs_;
    Query rhs_;
    std::string operator_symbol_;
};


#endif //TEXTQUERY_BINARY_QUERY_HH
