//
// Created by xiehao on 17-12-13.
//

#ifndef TEXTQUERY_TEXTQUERY_HH
#define TEXTQUERY_TEXTQUERY_HH


#include <vector>
#include <fstream>
#include <memory>
#include <unordered_map>
#include <set>

class QueryResult;

class TextQuery {
public:
    using line_number_type = std::vector<std::string>::size_type;
    using lines_type = std::set<line_number_type>;
    using lines_pointer_type = std::shared_ptr<lines_type>;

    explicit TextQuery(std::ifstream &is);

    QueryResult query(std::string const &sought) const;

private:
    std::shared_ptr<std::vector<std::string>> file_;
    std::unordered_map<std::string, lines_pointer_type> word_map_;
};


#endif //TEXTQUERY_TEXTQUERY_HH
