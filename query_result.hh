//
// Created by xiehao on 17-12-13.
//

#ifndef TEXTQUERY_QUERYRESULT_HH
#define TEXTQUERY_QUERYRESULT_HH


#include <memory>
#include <unordered_set>
#include <utility>

#include "text_query.hh"

class QueryResult {
    using file_pointer_type = std::shared_ptr<std::vector<std::string>>;
    friend std::ostream &print(std::ostream &, QueryResult const &);

public:
    QueryResult(std::string sought,
                TextQuery::lines_pointer_type p_lines,
                std::shared_ptr<std::vector<std::string>> p_file);

    TextQuery::lines_type::iterator begin();

    TextQuery::lines_type::iterator end();

    file_pointer_type file() const;

private:
    std::string sought_;
    TextQuery::lines_pointer_type p_lines_;
    file_pointer_type p_file_;
};


#endif //TEXTQUERY_QUERYRESULT_HH
