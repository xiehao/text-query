//
// Created by xiehao on 17-12-13.
//

#include "query_result.hh"

QueryResult::QueryResult(
        std::string sought,
        TextQuery::lines_pointer_type p_lines,
        std::shared_ptr<std::vector<std::string>> p_file)
        : sought_{std::move(sought)},
          p_lines_{std::move(p_lines)},
          p_file_{std::move(p_file)} {
}

TextQuery::lines_type::iterator QueryResult::end() {
    return p_lines_->end();
}

TextQuery::lines_type::iterator QueryResult::begin() {
    return p_lines_->begin();
}

QueryResult::file_pointer_type QueryResult::file() const {
    return p_file_;
}
