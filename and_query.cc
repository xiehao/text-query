//
// Created by oaheix on 14/12/2017.
//

#include "and_query.hh"
#include "query_result.hh"

using namespace std;

QueryResult AndQuery::evaluate(TextQuery const &text) const {
    auto right = rhs_.evaluate(text);
    auto left = lhs_.evaluate(text);
    auto return_lines = make_shared<lines_type>();
    set_intersection(left.begin(), left.end(), right.begin(), right.end(),
                     inserter(*return_lines, return_lines->begin()));
    return {representation(), return_lines, left.file()};
}
