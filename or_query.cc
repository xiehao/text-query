//
// Created by oaheix on 14/12/2017.
//

#include "or_query.hh"
#include "query_result.hh"

using namespace std;

QueryResult OrQuery::evaluate(TextQuery const &text) const {
    auto right = rhs_.evaluate(text);
    auto left = lhs_.evaluate(text);
    auto return_lines = make_shared<lines_type>(left.begin(), left.end());
    return_lines->insert(right.begin(), right.end());
    return {representation(), return_lines, left.file()};
}
