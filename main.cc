#include <iostream>

#include "text_query.hh"
#include "query_result.hh"
#include "query.hh"
#include "and_query.hh"
#include "or_query.hh"
#include "not_query.hh"

using namespace std;

string make_plural(size_t number, string const &word, string const &ending) {
    return (number > 1) ? word + ending : word;
}

ostream &print(ostream &os, QueryResult const &result) {
    os << result.sought_ << " occurs " << result.p_lines_->size() << " "
       << make_plural(result.p_lines_->size(), "time", "s") << endl;
    for (auto number : *result.p_lines_) {
        os << "\t(line " << number + 1 << ") "
           << *(result.p_file_->begin() + number) << endl;
    }
    return os;
}

void run_query(ifstream &file) {
    // 创建文本查询类对象，并传入待查文件
    TextQuery query{file};
    // 用户交互逻辑，提示用户输入查询单词
    while (true) {
        cout << "Enter word to look for, or q to quit:" << endl;
        string s;
        // 若到文件末尾或用户输入"q"则退出
        if (!(cin >> s) || "q" == s) { break; }
        // 打印查询结果
        print(cout, query.query(s));
    }
}

void run_combined_query(ifstream &file) {
    TextQuery text{file};
    Query q = (Query{"this"} | Query{"is"}) & ~Query{"a"};
    cout << q << endl;
    print(cout, q.evaluate(text));
}

int main() {
    ifstream file{"file_query.txt"};
    if (!file) { return EXIT_FAILURE; }
//    run_query(file);
    run_combined_query(file);
    return EXIT_SUCCESS;
}