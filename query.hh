//
// Created by oaheix on 14/12/2017.
//

#ifndef TEXTQUERY_QUERY_HH
#define TEXTQUERY_QUERY_HH


#include <utility>

#include "query_base.hh"

class Query {
    friend Query operator~(Query const &);

    friend Query operator|(Query const &, Query const &);

    friend Query operator&(Query const &, Query const &);

public:
    explicit Query(std::string const &);

    QueryResult evaluate(TextQuery const &query) const;

    std::string representation() const;

private:
    explicit Query(std::shared_ptr<QueryBase> p_query)
            : p_{std::move(p_query)} {}

    std::shared_ptr<QueryBase> p_;
};

inline std::ostream &operator<<(std::ostream &os, Query const &query) {
    return os << query.representation();
}

#endif //TEXTQUERY_QUERY_HH
