//
// Created by oaheix on 14/12/2017.
//

#ifndef TEXTQUERY_WORD_QUERY_HH
#define TEXTQUERY_WORD_QUERY_HH


#include <utility>

#include "query_base.hh"

class WordQuery : public QueryBase {
    friend class Query;

    explicit WordQuery(std::string word) : query_word_{std::move(word)} {}

    QueryResult evaluate(TextQuery const &text) const override;

    std::string representation() const override;

    std::string query_word_;
};


#endif //TEXTQUERY_WORD_QUERY_HH
