//
// Created by oaheix on 14/12/2017.
//

#include "word_query.hh"
#include "query_result.hh"

QueryResult WordQuery::evaluate(TextQuery const &text) const {
    return text.query(query_word_);
}

std::string WordQuery::representation() const {
    return query_word_;
}
