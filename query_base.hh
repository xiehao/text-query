//
// Created by oaheix on 14/12/2017.
//

#ifndef TEXTQUERY_QUERY_BASE_HH
#define TEXTQUERY_QUERY_BASE_HH

#include "text_query.hh"

class QueryBase {
    friend class Query;

protected:
    using line_number_type = TextQuery::line_number_type;
    using lines_type = TextQuery::lines_type;

    virtual ~QueryBase() = default;

private:
    virtual QueryResult evaluate(TextQuery const &) const = 0;

    virtual std::string representation() const = 0;
};


#endif //TEXTQUERY_QUERY_BASE_HH
