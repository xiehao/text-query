//
// Created by oaheix on 14/12/2017.
//

#include "query.hh"
#include "query_result.hh"
#include "word_query.hh"

using namespace std;

Query::Query(std::string const &word) : p_{new WordQuery{word}} {}

QueryResult Query::evaluate(TextQuery const &query) const {
    return p_->evaluate(query);
}

std::string Query::representation() const {
    return p_->representation();
}
