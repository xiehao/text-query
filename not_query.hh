//
// Created by oaheix on 14/12/2017.
//

#ifndef TEXTQUERY_NOT_QUERY_HH
#define TEXTQUERY_NOT_QUERY_HH


#include "query_base.hh"
#include "query.hh"

#include <utility>

class NotQuery : public QueryBase {
    friend Query operator~(Query const &);

    explicit NotQuery(Query query) : query_{std::move(query)} {}

    QueryResult evaluate(TextQuery const &text) const override;

    std::string representation() const override;

    Query query_;
};

inline Query operator~(Query const &operand) {
    return Query{std::shared_ptr<QueryBase>(new NotQuery{operand})};
}


#endif //TEXTQUERY_NOT_QUERY_HH
