//
// Created by oaheix on 14/12/2017.
//

#ifndef TEXTQUERY_AND_QUERY_HH
#define TEXTQUERY_AND_QUERY_HH


#include "binary_query.hh"

class AndQuery : public BinaryQuery {
    friend Query operator&(Query const &lhs, Query const &rhs);

    AndQuery(Query const &lhs, Query const &rhs)
            : BinaryQuery{lhs, rhs, "&"} {}

    QueryResult evaluate(TextQuery const &text) const override;
};

inline Query operator&(Query const &lhs, Query const &rhs) {
    return Query{std::shared_ptr<QueryBase>(new AndQuery{lhs, rhs})};
}


#endif //TEXTQUERY_AND_QUERY_HH
